/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-docker',
      type: 'Docker',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Docker = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Docker Adapter Test', () => {
  describe('Docker Class Tests', () => {
    const a = new Docker(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-docker-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-docker-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#getContainersjson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContainersjson('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainerscreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postContainerscreate('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('e90e34656806', data.response.Id);
                assert.equal(true, Array.isArray(data.response.Warnings));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainersidjson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContainersidjson('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('', data.response.AppArmorProfile);
                assert.equal(true, Array.isArray(data.response.Args));
                assert.equal('object', typeof data.response.Config);
                assert.equal('2015-01-06T15:47:31.485331387Z', data.response.Created);
                assert.equal('devicemapper', data.response.Driver);
                assert.equal('object', typeof data.response.HostConfig);
                assert.equal('/var/lib/docker/containers/ba033ac4401106a3b513bc9d639eee123ad78ca3616b921167cd74b20e25ed39/hostname', data.response.HostnamePath);
                assert.equal('/var/lib/docker/containers/ba033ac4401106a3b513bc9d639eee123ad78ca3616b921167cd74b20e25ed39/hosts', data.response.HostsPath);
                assert.equal('/var/lib/docker/containers/1eb5fabf5a03807136561b3c00adcd2992b535d624d5e18b6cdc6a6844d9767b/1eb5fabf5a03807136561b3c00adcd2992b535d624d5e18b6cdc6a6844d9767b-json.log', data.response.LogPath);
                assert.equal('ba033ac4401106a3b513bc9d639eee123ad78ca3616b921167cd74b20e25ed39', data.response.Id);
                assert.equal('04c5d3b7b0656168630d3ba35d8889bd0e9caafcaeb3004d2bfbc47e7c5d35d2', data.response.Image);
                assert.equal('', data.response.MountLabel);
                assert.equal('/boring_euclid', data.response.Name);
                assert.equal('object', typeof data.response.NetworkSettings);
                assert.equal('/bin/sh', data.response.Path);
                assert.equal('', data.response.ProcessLabel);
                assert.equal('/var/lib/docker/containers/ba033ac4401106a3b513bc9d639eee123ad78ca3616b921167cd74b20e25ed39/resolv.conf', data.response.ResolvConfPath);
                assert.equal(1, data.response.RestartCount);
                assert.equal('object', typeof data.response.State);
                assert.equal(true, Array.isArray(data.response.Mounts));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainersidtop - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContainersidtop('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.Titles));
                assert.equal(true, Array.isArray(data.response.Processes));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainersidlogs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getContainersidlogs('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainersidchanges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContainersidchanges('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainersidexport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getContainersidexport('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainersidstats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getContainersidstats('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('2015-01-08T22:57:31.547920715Z', data.response.read);
                assert.equal('object', typeof data.response.pids_stats);
                assert.equal('object', typeof data.response.networks);
                assert.equal('object', typeof data.response.memory_stats);
                assert.equal('object', typeof data.response.blkio_stats);
                assert.equal('object', typeof data.response.cpu_stats);
                assert.equal('object', typeof data.response.precpu_stats);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidresize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postContainersidresize('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidstart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postContainersidstart('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidstop - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postContainersidstop('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidrestart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postContainersidrestart('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidkill - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postContainersidkill('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidupdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postContainersidupdate('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.Warnings));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidrename - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postContainersidrename('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidpause - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postContainersidpause('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidunpause - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postContainersidunpause('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidattach - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postContainersidattach('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainersidattachws - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getContainersidattachws('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidwait - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postContainersidwait('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(10, data.response.StatusCode);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteContainersid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteContainersid('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#headContainersidarchive - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.headContainersidarchive('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getContainersidarchive - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getContainersidarchive('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putContainersidarchive - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putContainersidarchive('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersprune - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postContainersprune('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.ContainersDeleted));
                assert.equal(8, data.response.SpaceReclaimed);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImagesjson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImagesjson('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postBuild - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postBuild('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postImagescreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postImagescreate('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImagesnamejson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImagesnamejson('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('sha256:85f05633ddc1c50679be2b16a0479ab6f7637f8884e0cfe0f4d20e1ebb3d6e7c', data.response.Id);
                assert.equal('cb91e48a60d01f1e27028b4fc6819f4f290b3cf12496c8176ec714d0d390984a', data.response.Container);
                assert.equal('', data.response.Comment);
                assert.equal('linux', data.response.Os);
                assert.equal('amd64', data.response.Architecture);
                assert.equal('sha256:91e54dfb11794fad694460162bf0cb0a4fa710cfa3f60979c177d920813e267c', data.response.Parent);
                assert.equal('object', typeof data.response.ContainerConfig);
                assert.equal('1.9.0-dev', data.response.DockerVersion);
                assert.equal(188359297, data.response.VirtualSize);
                assert.equal(0, data.response.Size);
                assert.equal('', data.response.Author);
                assert.equal('2015-09-10T08:30:53.26995814Z', data.response.Created);
                assert.equal('object', typeof data.response.GraphDriver);
                assert.equal(true, Array.isArray(data.response.RepoDigests));
                assert.equal(true, Array.isArray(data.response.RepoTags));
                assert.equal('object', typeof data.response.Config);
                assert.equal('object', typeof data.response.RootFS);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImagesnamehistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImagesnamehistory('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postImagesnamepush - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postImagesnamepush('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postImagesnametag - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postImagesnametag('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteImagesname - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteImagesname('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImagessearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getImagessearch('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postImagesprune - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postImagesprune('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.ImagesDeleted));
                assert.equal(7, data.response.SpaceReclaimed);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postCommit - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postCommit('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.Id);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImagesnameget - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImagesnameget('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getImagesget - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getImagesget('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postImagesload - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postImagesload('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAuth('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('Login Succeeded', data.response.Status);
                assert.equal('9cbaf023786cd7...', data.response.IdentityToken);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInfo((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('x86_64', data.response.Architecture);
                assert.equal('etcd://localhost:2379', data.response.ClusterStore);
                assert.equal('cgroupfs', data.response.CgroupDriver);
                assert.equal(11, data.response.Containers);
                assert.equal(7, data.response.ContainersRunning);
                assert.equal(3, data.response.ContainersStopped);
                assert.equal(1, data.response.ContainersPaused);
                assert.equal(true, data.response.CpuCfsPeriod);
                assert.equal(true, data.response.CpuCfsQuota);
                assert.equal(false, data.response.Debug);
                assert.equal('/var/lib/docker', data.response.DockerRootDir);
                assert.equal('btrfs', data.response.Driver);
                assert.equal(true, Array.isArray(data.response.DriverStatus));
                assert.equal(false, data.response.ExperimentalBuild);
                assert.equal('http://test:test@localhost:8080', data.response.HttpProxy);
                assert.equal('https://test:test@localhost:8080', data.response.HttpsProxy);
                assert.equal('7TRN:IPZB:QYBB:VPBQ:UMPP:KARE:6ZNR:XE6T:7EWV:PKF4:ZOJD:TPYS', data.response.ID);
                assert.equal(true, data.response.IPv4Forwarding);
                assert.equal(16, data.response.Images);
                assert.equal('https://index.docker.io/v1/', data.response.IndexServerAddress);
                assert.equal('/usr/bin/docker', data.response.InitPath);
                assert.equal('', data.response.InitSha1);
                assert.equal(true, data.response.KernelMemory);
                assert.equal('3.12.0-1-amd64', data.response.KernelVersion);
                assert.equal(true, Array.isArray(data.response.Labels));
                assert.equal(2099236864, data.response.MemTotal);
                assert.equal(true, data.response.MemoryLimit);
                assert.equal(1, data.response.NCPU);
                assert.equal(0, data.response.NEventsListener);
                assert.equal(11, data.response.NFd);
                assert.equal(21, data.response.NGoroutines);
                assert.equal('prod-server-42', data.response.Name);
                assert.equal('9.81.1.160', data.response.NoProxy);
                assert.equal(true, data.response.OomKillDisable);
                assert.equal('linux', data.response.OSType);
                assert.equal('Boot2Docker', data.response.OperatingSystem);
                assert.equal('object', typeof data.response.Plugins);
                assert.equal('object', typeof data.response.RegistryConfig);
                assert.equal(true, Array.isArray(data.response.SecurityOptions));
                assert.equal('1.9.0', data.response.ServerVersion);
                assert.equal(false, data.response.SwapLimit);
                assert.equal(true, Array.isArray(data.response.SystemStatus));
                assert.equal('2015-03-10T11:11:23.730591467-07:00', data.response.SystemTime);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVersion((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('1.13.0', data.response.Version);
                assert.equal('linux', data.response.Os);
                assert.equal('3.19.0-23-generic', data.response.KernelVersion);
                assert.equal('go1.6.3', data.response.GoVersion);
                assert.equal('deadbee', data.response.GitCommit);
                assert.equal('amd64', data.response.Arch);
                assert.equal('1.25', data.response.ApiVersion);
                assert.equal('1.12', data.response.MinAPIVersion);
                assert.equal('2016-06-14T07:09:13.444803460+00:00', data.response.BuildTime);
                assert.equal(true, data.response.Experimental);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPing - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getPing((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getEvents('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('container', data.response.Type);
                assert.equal('create', data.response.Action);
                assert.equal('object', typeof data.response.Actor);
                assert.equal(1461943101, data.response.time);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemdf - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemdf((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(9, data.response.LayersSize);
                assert.equal(true, Array.isArray(data.response.Images));
                assert.equal(true, Array.isArray(data.response.Containers));
                assert.equal(true, Array.isArray(data.response.Volumes));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postContainersidexec - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postContainersidexec('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.Id);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExecidstart - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postExecidstart('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postExecidresize - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postExecidresize('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExecidjson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExecidjson('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(false, data.response.CanRemove);
                assert.equal('b53ee82b53a40c7dca428523e34f741f3abc51d9f297a14ff874bf761b995126', data.response.ContainerID);
                assert.equal('', data.response.DetachKeys);
                assert.equal(2, data.response.ExitCode);
                assert.equal('f33bbfb39f5b142420f4759b2348913bd4a8d1a6d7fd56499cb41a1bb91d7b3b', data.response.ID);
                assert.equal(true, data.response.OpenStderr);
                assert.equal(true, data.response.OpenStdin);
                assert.equal(true, data.response.OpenStdout);
                assert.equal('object', typeof data.response.ProcessConfig);
                assert.equal(false, data.response.Running);
                assert.equal(42000, data.response.Pid);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVolumes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVolumes('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.Volumes));
                assert.equal(true, Array.isArray(data.response.Warnings));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVolumescreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVolumescreate('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Driver);
                assert.equal('string', data.response.Mountpoint);
                assert.equal('object', typeof data.response.Status);
                assert.equal('object', typeof data.response.Labels);
                assert.equal('local', data.response.Scope);
                assert.equal('object', typeof data.response.Options);
                assert.equal('object', typeof data.response.UsageData);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVolumesname - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getVolumesname('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Driver);
                assert.equal('string', data.response.Mountpoint);
                assert.equal('object', typeof data.response.Status);
                assert.equal('object', typeof data.response.Labels);
                assert.equal('local', data.response.Scope);
                assert.equal('object', typeof data.response.Options);
                assert.equal('object', typeof data.response.UsageData);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteVolumesname - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteVolumesname('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVolumesprune - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVolumesprune('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.VolumesDeleted));
                assert.equal(8, data.response.SpaceReclaimed);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworks('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworksid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetworksid('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.Name);
                assert.equal('string', data.response.Id);
                assert.equal('string', data.response.Created);
                assert.equal('string', data.response.Scope);
                assert.equal('string', data.response.Driver);
                assert.equal(true, data.response.EnableIPv6);
                assert.equal('object', typeof data.response.IPAM);
                assert.equal(true, data.response.Internal);
                assert.equal('object', typeof data.response.Containers);
                assert.equal('object', typeof data.response.Options);
                assert.equal('object', typeof data.response.Labels);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworksid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNetworksid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkscreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNetworkscreate('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.Id);
                assert.equal('string', data.response.Warning);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworksidconnect - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNetworksidconnect('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworksiddisconnect - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNetworksiddisconnect('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworksprune - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNetworksprune('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.VolumesDeleted));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPlugins - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPlugins((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsprivileges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPluginsprivileges('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginspull - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPluginspull('fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPluginsnamejson - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPluginsnamejson('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.Id);
                assert.equal('string', data.response.Name);
                assert.equal(true, data.response.Enabled);
                assert.equal('object', typeof data.response.Settings);
                assert.equal('object', typeof data.response.Config);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePluginsname - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deletePluginsname('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.Id);
                assert.equal('string', data.response.Name);
                assert.equal(true, data.response.Enabled);
                assert.equal('object', typeof data.response.Settings);
                assert.equal('object', typeof data.response.Config);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsnameenable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPluginsnameenable('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsnamedisable - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPluginsnamedisable('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginscreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPluginscreate('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsnamepush - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPluginsnamepush('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postPluginsnameset - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postPluginsnameset('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodes('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNodesid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNodesid('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.ID);
                assert.equal('object', typeof data.response.Version);
                assert.equal('string', data.response.CreatedAt);
                assert.equal('string', data.response.UpdatedAt);
                assert.equal('object', typeof data.response.Spec);
                assert.equal('object', typeof data.response.Description);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNodesid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNodesid('fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNodesidupdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNodesidupdate('fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwarm - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSwarm((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.ID);
                assert.equal('object', typeof data.response.Version);
                assert.equal('string', data.response.CreatedAt);
                assert.equal('string', data.response.UpdatedAt);
                assert.equal('object', typeof data.response.Spec);
                assert.equal('object', typeof data.response.JoinTokens);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSwarminit - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSwarminit('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSwarmjoin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSwarmjoin('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSwarmleave - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSwarmleave('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSwarmupdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSwarmupdate('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSwarmunlockkey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSwarmunlockkey((data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.UnlockKey);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSwarmunlock - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postSwarmunlock('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServices('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postServicescreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postServicescreate('fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.ID);
                assert.equal('string', data.response.Warning);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServicesid('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.ID);
                assert.equal('object', typeof data.response.Version);
                assert.equal('string', data.response.CreatedAt);
                assert.equal('string', data.response.UpdatedAt);
                assert.equal('object', typeof data.response.Spec);
                assert.equal('object', typeof data.response.Endpoint);
                assert.equal('object', typeof data.response.UpdateStatus);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteServicesid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteServicesid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postServicesidupdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postServicesidupdate('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.Untagged);
                assert.equal('string', data.response.Deleted);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServicesidlogs - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getServicesidlogs('fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', 'fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTasks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTasks('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTasksid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTasksid('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.ID);
                assert.equal('object', typeof data.response.Version);
                assert.equal('string', data.response.CreatedAt);
                assert.equal('string', data.response.UpdatedAt);
                assert.equal('string', data.response.Name);
                assert.equal('object', typeof data.response.Labels);
                assert.equal('object', typeof data.response.Spec);
                assert.equal('string', data.response.ServiceID);
                assert.equal(6, data.response.Slot);
                assert.equal('string', data.response.NodeID);
                assert.equal('object', typeof data.response.Status);
                assert.equal('assigned', data.response.DesiredState);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecrets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecrets('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSecretscreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSecretscreate('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.ID);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSecretsid - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSecretsid('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.ID);
                assert.equal('object', typeof data.response.Version);
                assert.equal('string', data.response.CreatedAt);
                assert.equal('string', data.response.UpdatedAt);
                assert.equal('object', typeof data.response.Spec);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSecretsid - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSecretsid('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                assert.equal(undefined, error);
                assert.notEqual(undefined, data);
                assert.notEqual(null, data);
                assert.notEqual(null, data.response);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
