# Adapter for Docker

Vendor: Docker
Homepage: https://www.docker.com/

Product: Docker Engine
Product Page: https://docs.docker.com/engine/

## Introduction
We classify Docker into the Cloud domain as Docker provides the capabiity to interact with Docker containers and services which are deployed and managed in cloud environment.

## Why Integrate
The Docker adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Docker Engine. With this adapter you have the ability to perform operations such as:

- Pull, Push, or Manage Docker Images.

- Add and Remove Inventory Items: When a new device is turned up, it can be automatically added to inventory as well as having its assets (e.g. ports) added. When a device is removed, it can be automatically removed from inventory as well as having its assets removed.

- Check Availability of Items: When a new service is requested, Itential can confirm the availability of the items that are required to provide the service.

- Assign and Unassign Items: When a new service is requested, the items required to provide the service such as a port can be assigned to the service. When a service is discontinued, the items that were utilized for that service can be un-assigned so they can be used again in the future.

## Additional Product Documentation
The [API documents for Docker](https://docs.docker.com/engine/api/)

