# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Docker System. The API that was used to build the adapter for Docker is usually available in the report directory of this adapter. The adapter utilizes the Docker API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Docker adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Docker Engine. With this adapter you have the ability to perform operations such as:

- Pull, Push, or Manage Docker Images.

- Add and Remove Inventory Items: When a new device is turned up, it can be automatically added to inventory as well as having its assets (e.g. ports) added. When a device is removed, it can be automatically removed from inventory as well as having its assets removed.

- Check Availability of Items: When a new service is requested, Itential can confirm the availability of the items that are required to provide the service.

- Assign and Unassign Items: When a new service is requested, the items required to provide the service such as a port can be assigned to the service. When a service is discontinued, the items that were utilized for that service can be un-assigned so they can be used again in the future.

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
