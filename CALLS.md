## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Docker. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Docker.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Docker. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getContainersjson(all, limit, size, filters, callback)</td>
    <td style="padding:15px">List containers</td>
    <td style="padding:15px">{base_path}/{version}/containers/json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainerscreate(name, body, callback)</td>
    <td style="padding:15px">Create a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainersidjson(id, size, callback)</td>
    <td style="padding:15px">Inspect a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainersidtop(id, psArgs, callback)</td>
    <td style="padding:15px">List processes running inside a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/top?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainersidlogs(id, follow, stdout, stderr, since, timestamps, tail, callback)</td>
    <td style="padding:15px">Get container logs</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainersidchanges(id, callback)</td>
    <td style="padding:15px">Get changes on a container’s filesystem</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainersidexport(id, callback)</td>
    <td style="padding:15px">Export a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/export?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainersidstats(id, stream, callback)</td>
    <td style="padding:15px">Get container stats based on resource usage</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/stats?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidresize(id, h, w, callback)</td>
    <td style="padding:15px">Resize a container TTY</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/resize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidstart(id, detachKeys, callback)</td>
    <td style="padding:15px">Start a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidstop(id, t, callback)</td>
    <td style="padding:15px">Stop a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidrestart(id, t, callback)</td>
    <td style="padding:15px">Restart a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/restart?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidkill(id, signal, callback)</td>
    <td style="padding:15px">Kill a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/kill?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidupdate(id, update, callback)</td>
    <td style="padding:15px">Update a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidrename(id, name, callback)</td>
    <td style="padding:15px">Rename a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/rename?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidpause(id, callback)</td>
    <td style="padding:15px">Pause a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/pause?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidunpause(id, callback)</td>
    <td style="padding:15px">Unpause a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/unpause?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidattach(id, detachKeys, logs, stream, stdin, stdout, stderr, callback)</td>
    <td style="padding:15px">Attach to a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/attach?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainersidattachws(id, detachKeys, logs, stream, stdin, stdout, stderr, callback)</td>
    <td style="padding:15px">Attach to a container via a websocket</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/attach/ws?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidwait(id, callback)</td>
    <td style="padding:15px">Wait for a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/wait?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteContainersid(id, v, force, callback)</td>
    <td style="padding:15px">Remove a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">headContainersidarchive(id, path_resource, callback)</td>
    <td style="padding:15px">Get information about files in a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContainersidarchive(id, path_resource, callback)</td>
    <td style="padding:15px">Get an archive of a filesystem resource in a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putContainersidarchive(id, path_dir, noOverwriteDirNonDir, inputStream, callback)</td>
    <td style="padding:15px">Extract an archive of files or folders to a directory in a container</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/archive?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersprune(filters, callback)</td>
    <td style="padding:15px">Delete stopped containers</td>
    <td style="padding:15px">{base_path}/{version}/containers/prune?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImagesjson(all, filters, digests, callback)</td>
    <td style="padding:15px">List Images</td>
    <td style="padding:15px">{base_path}/{version}/images/json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postBuild(inputStream, dockerfile, t, remote, q, nocache, cachefrom, pull, rm, forcerm, memory, memswap, cpushares, cpusetcpus, cpuperiod, cpuquota, buildargs, shmsize, squash, labels, networkmode, Contenttype, XRegistryConfig, callback)</td>
    <td style="padding:15px">Build an image</td>
    <td style="padding:15px">{base_path}/{version}/build?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postImagescreate(fromImage, fromSrc, repo, tag, inputImage, XRegistryAuth, callback)</td>
    <td style="padding:15px">Create an image</td>
    <td style="padding:15px">{base_path}/{version}/images/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImagesnamejson(name, callback)</td>
    <td style="padding:15px">Inspect an image</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImagesnamehistory(name, callback)</td>
    <td style="padding:15px">Get the history of an image</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postImagesnamepush(name, tag, XRegistryAuth, callback)</td>
    <td style="padding:15px">Push an image</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postImagesnametag(name, repo, tag, callback)</td>
    <td style="padding:15px">Tag an image</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteImagesname(name, force, noprune, callback)</td>
    <td style="padding:15px">Remove an image</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImagessearch(term, limit, filters, callback)</td>
    <td style="padding:15px">Search images</td>
    <td style="padding:15px">{base_path}/{version}/images/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postImagesprune(filters, callback)</td>
    <td style="padding:15px">Delete unused images</td>
    <td style="padding:15px">{base_path}/{version}/images/prune?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCommit(containerConfig, container, repo, tag, comment, author, pause, changes, callback)</td>
    <td style="padding:15px">Create a new image from a container</td>
    <td style="padding:15px">{base_path}/{version}/commit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImagesnameget(name, callback)</td>
    <td style="padding:15px">Export an image</td>
    <td style="padding:15px">{base_path}/{version}/images/{pathv1}/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getImagesget(names, callback)</td>
    <td style="padding:15px">Export several images</td>
    <td style="padding:15px">{base_path}/{version}/images/get?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postImagesload(imagesTarball, quiet, callback)</td>
    <td style="padding:15px">Import images</td>
    <td style="padding:15px">{base_path}/{version}/images/load?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuth(authConfig, callback)</td>
    <td style="padding:15px">Check auth configuration</td>
    <td style="padding:15px">{base_path}/{version}/auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInfo(callback)</td>
    <td style="padding:15px">Get system information</td>
    <td style="padding:15px">{base_path}/{version}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVersion(callback)</td>
    <td style="padding:15px">Get version</td>
    <td style="padding:15px">{base_path}/{version}/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPing(callback)</td>
    <td style="padding:15px">Ping</td>
    <td style="padding:15px">{base_path}/{version}/_ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEvents(since, until, filters, callback)</td>
    <td style="padding:15px">Monitor events</td>
    <td style="padding:15px">{base_path}/{version}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemdf(callback)</td>
    <td style="padding:15px">Get data usage information</td>
    <td style="padding:15px">{base_path}/{version}/system/df?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContainersidexec(execConfig, id, callback)</td>
    <td style="padding:15px">Create an exec instance</td>
    <td style="padding:15px">{base_path}/{version}/containers/{pathv1}/exec?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExecidstart(execStartConfig, id, callback)</td>
    <td style="padding:15px">Start an exec instance</td>
    <td style="padding:15px">{base_path}/{version}/exec/{pathv1}/start?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postExecidresize(id, h, w, callback)</td>
    <td style="padding:15px">Resize an exec instance</td>
    <td style="padding:15px">{base_path}/{version}/exec/{pathv1}/resize?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExecidjson(id, callback)</td>
    <td style="padding:15px">Inspect an exec instance</td>
    <td style="padding:15px">{base_path}/{version}/exec/{pathv1}/json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVolumes(filters, callback)</td>
    <td style="padding:15px">List volumes</td>
    <td style="padding:15px">{base_path}/{version}/volumes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVolumescreate(volumeConfig, callback)</td>
    <td style="padding:15px">Create a volume</td>
    <td style="padding:15px">{base_path}/{version}/volumes/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVolumesname(name, callback)</td>
    <td style="padding:15px">Inspect a volume</td>
    <td style="padding:15px">{base_path}/{version}/volumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVolumesname(name, force, callback)</td>
    <td style="padding:15px">Remove a volume</td>
    <td style="padding:15px">{base_path}/{version}/volumes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVolumesprune(filters, callback)</td>
    <td style="padding:15px">Delete unused volumes</td>
    <td style="padding:15px">{base_path}/{version}/volumes/prune?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworks(filters, callback)</td>
    <td style="padding:15px">List networks</td>
    <td style="padding:15px">{base_path}/{version}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetworksid(id, callback)</td>
    <td style="padding:15px">Inspect a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworksid(id, callback)</td>
    <td style="padding:15px">Remove a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworkscreate(networkConfig, callback)</td>
    <td style="padding:15px">Create a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworksidconnect(id, container, callback)</td>
    <td style="padding:15px">Connect a container to a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworksiddisconnect(id, container, callback)</td>
    <td style="padding:15px">Disconnect a container from a network</td>
    <td style="padding:15px">{base_path}/{version}/networks/{pathv1}/disconnect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNetworksprune(filters, callback)</td>
    <td style="padding:15px">Delete unused networks</td>
    <td style="padding:15px">{base_path}/{version}/networks/prune?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPlugins(callback)</td>
    <td style="padding:15px">List plugins</td>
    <td style="padding:15px">{base_path}/{version}/plugins?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsprivileges(name, callback)</td>
    <td style="padding:15px">Get plugin privileges</td>
    <td style="padding:15px">{base_path}/{version}/plugins/privileges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginspull(remote, name, XRegistryAuth, body, callback)</td>
    <td style="padding:15px">Install a plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/pull?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPluginsnamejson(name, callback)</td>
    <td style="padding:15px">Inspect a plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}/json?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePluginsname(name, force, callback)</td>
    <td style="padding:15px">Remove a plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsnameenable(name, timeout, callback)</td>
    <td style="padding:15px">Enable a plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}/enable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsnamedisable(name, callback)</td>
    <td style="padding:15px">Disable a plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}/disable?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginscreate(name, tarContext, callback)</td>
    <td style="padding:15px">Create a plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsnamepush(name, callback)</td>
    <td style="padding:15px">Push a plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}/push?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postPluginsnameset(name, body, callback)</td>
    <td style="padding:15px">Configure a plugin</td>
    <td style="padding:15px">{base_path}/{version}/plugins/{pathv1}/set?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodes(filters, callback)</td>
    <td style="padding:15px">List nodes</td>
    <td style="padding:15px">{base_path}/{version}/nodes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNodesid(id, callback)</td>
    <td style="padding:15px">Inspect a node</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNodesid(id, force, callback)</td>
    <td style="padding:15px">Delete a node</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNodesidupdate(id, body, version, callback)</td>
    <td style="padding:15px">Update a node</td>
    <td style="padding:15px">{base_path}/{version}/nodes/{pathv1}/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwarm(callback)</td>
    <td style="padding:15px">Inspect swarm</td>
    <td style="padding:15px">{base_path}/{version}/swarm?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSwarminit(body, callback)</td>
    <td style="padding:15px">Initialize a new swarm</td>
    <td style="padding:15px">{base_path}/{version}/swarm/init?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSwarmjoin(body, callback)</td>
    <td style="padding:15px">Join an existing swarm</td>
    <td style="padding:15px">{base_path}/{version}/swarm/join?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSwarmleave(force, callback)</td>
    <td style="padding:15px">Leave a swarm</td>
    <td style="padding:15px">{base_path}/{version}/swarm/leave?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSwarmupdate(body, version, rotateWorkerToken, rotateManagerToken, rotateManagerUnlockKey, callback)</td>
    <td style="padding:15px">Update a swarm</td>
    <td style="padding:15px">{base_path}/{version}/swarm/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSwarmunlockkey(callback)</td>
    <td style="padding:15px">Get the unlock key</td>
    <td style="padding:15px">{base_path}/{version}/swarm/unlockkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSwarmunlock(body, callback)</td>
    <td style="padding:15px">Unlock a locked manager</td>
    <td style="padding:15px">{base_path}/{version}/swarm/unlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(filters, callback)</td>
    <td style="padding:15px">List services</td>
    <td style="padding:15px">{base_path}/{version}/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServicescreate(body, XRegistryAuth, callback)</td>
    <td style="padding:15px">Create a service</td>
    <td style="padding:15px">{base_path}/{version}/services/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesid(id, callback)</td>
    <td style="padding:15px">Inspect a service</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServicesid(id, callback)</td>
    <td style="padding:15px">Delete a service</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postServicesidupdate(id, body, version, registryAuthFrom, XRegistryAuth, callback)</td>
    <td style="padding:15px">Update a service</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServicesidlogs(id, details, follow, stdout, stderr, since, timestamps, tail, callback)</td>
    <td style="padding:15px">Get service logs</td>
    <td style="padding:15px">{base_path}/{version}/services/{pathv1}/logs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTasks(filters, callback)</td>
    <td style="padding:15px">List tasks</td>
    <td style="padding:15px">{base_path}/{version}/tasks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTasksid(id, callback)</td>
    <td style="padding:15px">Inspect a task</td>
    <td style="padding:15px">{base_path}/{version}/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecrets(filters, callback)</td>
    <td style="padding:15px">List secrets</td>
    <td style="padding:15px">{base_path}/{version}/secrets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSecretscreate(body, callback)</td>
    <td style="padding:15px">Create a secret</td>
    <td style="padding:15px">{base_path}/{version}/secrets/create?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSecretsid(id, callback)</td>
    <td style="padding:15px">Inspect a secret</td>
    <td style="padding:15px">{base_path}/{version}/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSecretsid(id, callback)</td>
    <td style="padding:15px">Delete a secret</td>
    <td style="padding:15px">{base_path}/{version}/secrets/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
