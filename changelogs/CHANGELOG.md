
## 0.6.0 [05-21-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/cloud/adapter-docker!9

---

## 0.5.4 [03-04-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/cloud/adapter-docker!8

---

## 0.5.3 [07-07-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-docker!7

---

## 0.5.2 [01-09-2020]

- Bring the adapter up to the latest foundation

See merge request itentialopensource/adapters/cloud/adapter-docker!6

---

## 0.5.1 [11-18-2019]

- Changes the healthcheck entity path to a path we know works. Also clean up the format of the entitypaths in general.

See merge request itentialopensource/adapters/cloud/adapter-docker!5

---

## 0.5.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/cloud/adapter-docker!4

---

## 0.4.0 [09-13-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/cloud/adapter-docker!3

---

## 0.3.0 [07-30-2019] & 0.2.0 [07-18-2019]

- Migrate to the latest adapter foundation, categorize and prepare for app artifact

See merge request itentialopensource/adapters/cloud/adapter-docker!2

---

## 0.1.2 [04-23-2019]

- Added keys to the repo and fixed the versions that the adapter was built with

See merge request itentialopensource/adapters/adapter-docker!1

---

## 0.1.1 [04-23-2019]

- Initial Commit

See commit e4666d2

---
